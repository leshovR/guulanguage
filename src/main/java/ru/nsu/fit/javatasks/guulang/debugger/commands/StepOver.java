package ru.nsu.fit.javatasks.guulang.debugger.commands;

import ru.nsu.fit.javatasks.guulang.debugger.view.View;
import ru.nsu.fit.javatasks.guulang.interpreter.Interpreter;

public class StepOver implements DebuggerCommand {
    public void execute(Interpreter interpreter, View view) {
        int next = interpreter.getNextCommandAddr();
        while (!interpreter.finished() &&
                interpreter.getContext().counter != next)
        {
            interpreter.runNext();
        }
        if (interpreter.getNextInstruction() == null) {
            interpreter.runNext();
        }
    }
}
