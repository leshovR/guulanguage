package ru.nsu.fit.javatasks.guulang.operators;

import ru.nsu.fit.javatasks.guulang.interpreter.ExecutionContext;

import java.io.PrintStream;
import java.util.Arrays;

public class Print implements Operator {


    @Override
    public void execute(ExecutionContext context, Object[] args) {
        if (args.length != 1) {
            throw new IllegalArgumentException(
                    "Wrong instruction format. Instruction: printLine, args: " +
                            Arrays.toString(args));
        }
        PrintStream ps = new PrintStream(context.out);
        if (args[0] instanceof Integer) {
            int value = (int) args[0];
            ps.println(value);
        } else if (args[0] instanceof String) {
            if (!context.defineTable.containsKey(args[0])) {
                throw new IllegalArgumentException("Wrong argument format: \"" + args[0] + "\"");
            }
            int value = context.defineTable.get(args[0]);
            ps.println(value);
        } else {
            throw new IllegalArgumentException("Wrong argument format: \"" + args[0] + "\"");
        }
    }
}
