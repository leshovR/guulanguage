package ru.nsu.fit.javatasks.guulang.debugger.view;

import java.io.*;

public class IOStreamView implements View {
    private BufferedReader in;
    private PrintStream out;

    public IOStreamView(InputStream in, OutputStream out) {
        this.in = new BufferedReader(new InputStreamReader(in));
        this.out = new PrintStream(out);
    }

    @Override
    public void printLine(String line) {
        out.println(line);
    }

    @Override
    public String readLine() throws IOException {
        return in.readLine();
    }

    @Override
    public boolean finished() {
        return false;
    }

    @Override
    public OutputStream getOutputStream() {
        return out;
    }
}
