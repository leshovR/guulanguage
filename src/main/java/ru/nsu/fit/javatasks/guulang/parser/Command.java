package ru.nsu.fit.javatasks.guulang.parser;

import ru.nsu.fit.javatasks.guulang.interpreter.ExecutionContext;
import ru.nsu.fit.javatasks.guulang.operators.Operator;

import java.util.List;


/*
 * Represents an executable instruction
 */
public class Command implements Instruction {
    private Operator operator;
    private List<Object> args;
    private int lineNumber;

    public Command(Operator operator, List<Object> args) {
        this.operator = operator;
        this.args = args;
    }

    @Override
    public InstructionType getType() {
        return InstructionType.COMMAND;
    }

    @Override
    public List<Object> getArgs() {
        return args;
    }

    @Override
    public String getName() {
        return operator.getClass().getSimpleName().toLowerCase();
    }

    @Override
    public int getLineNumber() {
        return lineNumber;
    }

    @Override
    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public void execute(ExecutionContext context) {
        operator.execute(context, args.toArray());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getName());
        for (Object arg : getArgs()) {
            sb.append(" ").append(arg);
        }
        return sb.toString();
    }
}
