package ru.nsu.fit.javatasks.guulang.lexer;

import org.junit.jupiter.api.Test;
import ru.nsu.fit.javatasks.guulang.operators.OperatorFactory;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class LexerTest {
    private Lexer lexer;

    public LexerTest() throws FileNotFoundException, InstantiationException {
        OperatorFactory.initialize("operators.properties");
        lexer = new Lexer();
    }


    @Test
    public void shouldReturnEmptyListWhenPutEmptyExpression() {
        List<Token> tokens = lexer.tokenize("");
        assertTrue(tokens.isEmpty());
    }


    @Test
    public void shouldTokenizeSingleOperators() {
        for (String name : OperatorFactory.getOperatorNames()) {
            List<Token> tokens = lexer.tokenize(name);
            assertEquals(1, tokens.size());
            Token token = tokens.get(0);
            assertNotNull(token);
            assertEquals(TokenType.OPERATOR, token.getType());
            assertEquals(name, token.getValue());
        }
    }


    @Test
    public void shouldTokenizeSingleDigit() {
        List<Token> tokens = lexer.tokenize("1");
        assertEquals(tokens.size(), 1);
        Token token = tokens.get(0);
        assertEquals(new Token(TokenType.NUMBER, 1), token);
    }


    @Test
    public void shouldTokenizeMultiDigitNumber() {
        List<Token> tokens = lexer.tokenize("1234");
        assertEquals(tokens.size(), 1);
        Token token = tokens.get(0);
        assertEquals(new Token(TokenType.NUMBER, 1234), token);
    }

    @Test
    public void shouldTokenizeSignedPositiveNumber() {
        List<Token> tokens = lexer.tokenize("+1");
        assertEquals(tokens.size(), 1);
        Token token = tokens.get(0);
        assertEquals(new Token(TokenType.NUMBER, 1), token);
    }

    @Test
    public void shouldTokenizeSignedNegativeNumber() {
        List<Token> tokens = lexer.tokenize("-1");
        assertEquals(tokens.size(), 1);
        Token token = tokens.get(0);
        assertEquals(new Token(TokenType.NUMBER, -1), token);
    }


    @Test
    public void shouldTokenizeOperatorAndNumber() {
        List<Token> tokens = lexer.tokenize("set 1");
        List<Token> expected = Arrays.asList(
                new Token(TokenType.OPERATOR, "set"),
                new Token(TokenType.NUMBER, 1));
        assertArrayEquals(tokens.toArray(), expected.toArray());
    }

    @Test
    public void shouldSkipSpaces() {
        List<Token> tokens = lexer.tokenize("1  \n-12    set\tcall");

        List<Token> expected = Arrays.asList(
                new Token(TokenType.NUMBER, 1),
                new Token(TokenType.NUMBER, -12),
                new Token(TokenType.OPERATOR, "set"),
                new Token(TokenType.OPERATOR, "call"));

        assertArrayEquals(tokens.toArray(), expected.toArray());
    }
}