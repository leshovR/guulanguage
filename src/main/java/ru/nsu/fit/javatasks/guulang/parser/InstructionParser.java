package ru.nsu.fit.javatasks.guulang.parser;

import ru.nsu.fit.javatasks.guulang.lexer.Token;
import ru.nsu.fit.javatasks.guulang.lexer.TokenType;
import ru.nsu.fit.javatasks.guulang.operators.Operator;
import ru.nsu.fit.javatasks.guulang.operators.OperatorFactory;

import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;

/*
 * Converts lexer output
 * to the instruction.
 * Should be given a single expression
 */
public class InstructionParser {
    public Instruction parse(List<Token> expression) throws IllegalFormatException {
        if (expression.isEmpty()) return null;

        Token op = expression.get(0);

        if (op.getType() == TokenType.NAME) {
            throw new IllegalArgumentException("Wrong expression format");
        }

        List<Object> args = new ArrayList<>();
        for (int i = 1; i < expression.size(); ++i) {
            Token token = expression.get(i);
            if (token.getType() == TokenType.OPERATOR) {
                throw new IllegalArgumentException("Wrong expression format");
            }
            if (token.getType() == TokenType.NAME) {
                if (!isValidName((String) token.getValue())) {
                    throw new IllegalArgumentException("Wrong identifier format: " +
                            token.getValue());
                }
            }
            args.add(token.getValue());
        }

        if (op.getType() == TokenType.OPERATOR) {
            Operator operator = OperatorFactory.getInstance((String) op.getValue());
            return new Command(operator, args);
        } else if(op.getType() == TokenType.KEYWORD) {
            String kwdname = (String)op.getValue();
            return new Keyword(kwdname, args);
        } else {
            throw new RuntimeException("This shouldn't have happened");
        }

    }

    private boolean isValidName(String name) {
        return name.matches("[\\w&&\\D]\\w*");
    }
}
