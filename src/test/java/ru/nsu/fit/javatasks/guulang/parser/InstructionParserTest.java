package ru.nsu.fit.javatasks.guulang.parser;

import org.junit.jupiter.api.Test;
import ru.nsu.fit.javatasks.guulang.lexer.Token;
import ru.nsu.fit.javatasks.guulang.lexer.TokenType;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class InstructionParserTest {
    private InstructionParser instructionParser;

    public InstructionParserTest() {
        instructionParser = new InstructionParser();
    }

    @Test
    public void shouldReturnNullWhenPutEmptyExpression() {
        Instruction inst = instructionParser.parse(Collections.emptyList());
        assertNull(inst);
    }

    @Test
    public void shouldReturnCorrectPrintInstruction() {
        Instruction inst = instructionParser.parse(
                Arrays.asList(new Token(TokenType.OPERATOR, "print"),
                        new Token(TokenType.NUMBER, 1)));

        assertNotNull(inst);
    }

    @Test
    public void shouldParseText() {

    }
}