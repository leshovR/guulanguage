package ru.nsu.fit.javatasks.guulang.debugger;

import ru.nsu.fit.javatasks.guulang.debugger.commands.DebugCommandFactory;
import ru.nsu.fit.javatasks.guulang.debugger.commands.DebuggerCommand;
import ru.nsu.fit.javatasks.guulang.debugger.view.ConsoleView;
import ru.nsu.fit.javatasks.guulang.debugger.view.View;
import ru.nsu.fit.javatasks.guulang.interpreter.Interpreter;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


/*
 * Step by step code executor.
 * Reacts to commands listed in the file "debugger.properties"
 */
public class Debugger {
    private View view;
    private Interpreter interpreter;

    public Debugger() throws FileNotFoundException, InstantiationException {
        DebugCommandFactory.initialize("debugger.properties");
        interpreter = new Interpreter();
        view = new ConsoleView();
        interpreter.getContext().out = view.getOutputStream();
    }

    public void run(String filename) throws IOException {
        view.printLine("Starting interpretation of the file " + filename);

        try {
            interpreter.interpret(new BufferedInputStream(new FileInputStream(filename)));
            while (!interpreter.finished() && !view.finished()) {
                view.printLine("Current instruction: " + interpreter.getNextInstruction());
                String userInput = view.readLine();
                DebuggerCommand command = DebugCommandFactory.getInstance(userInput);
                if (command == null) {
                    view.printLine("Unknown command: " + userInput);
                    continue;
                }

                command.execute(interpreter, view);
            }
        } catch (FileNotFoundException e) {
            view.printLine("Failed to initialize interpreter");
        } catch (IllegalArgumentException e) {
            view.printLine("Syntax error" + e.getMessage());
        }

        view.printLine("Interpretation finished");
    }
}
