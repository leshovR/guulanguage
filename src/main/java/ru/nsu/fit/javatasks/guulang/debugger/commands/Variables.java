package ru.nsu.fit.javatasks.guulang.debugger.commands;

import ru.nsu.fit.javatasks.guulang.debugger.view.View;
import ru.nsu.fit.javatasks.guulang.interpreter.Interpreter;

import java.io.PrintStream;
import java.util.Map;

public class Variables implements DebuggerCommand {
    public void execute(Interpreter interpreter, View view) {
        PrintStream ps = new PrintStream(interpreter.getContext().out);

        if (interpreter.getVariables().isEmpty()) {
            ps.println("No variables");
            return;
        }

        for (Map.Entry<String, Integer> entry : interpreter.getVariables()) {
            ps.println(entry.getKey() + " = " + entry.getValue());
        }

    }
}
