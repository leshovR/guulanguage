package ru.nsu.fit.javatasks.guulang.interpreter;


/*
 * The stack of subroutine calls.
 * Stores subroutine names and return addresses
 */
public class ExecutionStack {
    private StackFrame[] stack;
    private int stackPtr;

    public ExecutionStack(int size) {
        stackPtr = size;
        stack = new StackFrame[stackPtr];
    }

    public void push(StackFrame frame) throws RuntimeException {
        if (stackPtr == 0) throw new RuntimeException("Stack overflow");
        stack[--stackPtr] = frame;
    }

    public StackFrame pop() throws RuntimeException {
        if (stackPtr == stack.length) throw new RuntimeException("Stack underflow");
        return stack[stackPtr++];
    }

    public StackFrame top() {
        if (stackPtr < stack.length)
            return stack[stackPtr];
        else throw new RuntimeException("Stack underflow");
    }

    public boolean isEmpty() {
        return stackPtr == stack.length;
    }
}
