package ru.nsu.fit.javatasks.guulang.operators;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
/*
 * Used to get an operator class instance.
 * Loads operator names from the file.
 */
public class OperatorFactory {
    private static HashMap<String, Operator> operators = new HashMap<>();

    private OperatorFactory() { }

    public static void initialize(String filename) throws FileNotFoundException, InstantiationException {
        Properties properties = new Properties();
        try {
            InputStream is = OperatorFactory.class.getClassLoader()
                    .getResourceAsStream(filename);

            properties.load(is);
            for (String opName : properties.stringPropertyNames()) {
                Class opClass = Class.forName(properties.getProperty(opName));
                Operator operator = (Operator) opClass.newInstance();
                operators.put(opName, operator);
            }
        } catch (IOException e) {
            FileNotFoundException fe = new FileNotFoundException("Can't initialize lexer");
            fe.addSuppressed(e);
            throw fe;
        } catch (Exception e) {
            InstantiationException ie = new InstantiationException("Invalid operators properties file");
            ie.addSuppressed(e);
            throw ie;
        }
    }

    public static Operator getInstance(String name) {
        return operators.getOrDefault(name, null);
    }

    public static Set<String> getOperatorNames() {
        return operators.keySet();
    }
}
