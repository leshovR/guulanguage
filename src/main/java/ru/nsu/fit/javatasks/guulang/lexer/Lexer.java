package ru.nsu.fit.javatasks.guulang.lexer;

import ru.nsu.fit.javatasks.guulang.operators.OperatorFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
 * Lexer breaks lines of code down to lexemes
 * Result of its work is the list of tokens
 */
public class Lexer {
    private Set<String> keywords = new HashSet<>();
    public Lexer() {
        keywords.add("sub");
        // Place for expansion
    }


    public List<Token> tokenize(String line) {
        String[] words = line.split("\\s+");
        List<Token> result = new ArrayList<>();
        for (String word : words) {
            if (word.isEmpty()) continue;

            Token token;
            if (isNumber(word)) {
                token = new Token(TokenType.NUMBER, Integer.parseInt(word));
            } else if (isOperator(word)) {
                token = new Token(TokenType.OPERATOR, word);
            } else if (isKeyword(word)) {
                token = new Token(TokenType.KEYWORD, word);
            } else {
                token = new Token(TokenType.NAME, word);
            }
            result.add(token);
        }
        return result;
    }

    private boolean isNumber(String word) {
        return word.matches("[+-]?[0-9]+");
    }

    private boolean isOperator(String word) {
        return OperatorFactory.getOperatorNames().contains(word);
    }

    private boolean isKeyword(String word) {
        return keywords.contains(word);
    }
}
