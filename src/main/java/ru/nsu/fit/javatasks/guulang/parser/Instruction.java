package ru.nsu.fit.javatasks.guulang.parser;

import java.util.List;

/*
 * Line of code translated into the inner representation.
 */
public interface Instruction {
    InstructionType getType();
    List<Object> getArgs();
    String getName();
    int getLineNumber();
    void setLineNumber(int number);
}
