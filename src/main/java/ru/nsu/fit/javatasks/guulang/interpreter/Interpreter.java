package ru.nsu.fit.javatasks.guulang.interpreter;

import ru.nsu.fit.javatasks.guulang.lexer.Lexer;
import ru.nsu.fit.javatasks.guulang.operators.OperatorFactory;
import ru.nsu.fit.javatasks.guulang.parser.*;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.*;

/*
 * Uses lexer and parsers to translate text into the program.
 * Manages execution process.
 */
public class Interpreter {
    private ExecutionContext context;

    private InstructionParser instructionParser;
    private SubroutineParser subroutineParser;
    private Lexer lexer;
    private ArrayList<Instruction> program;
    private boolean finished = false;

    public Interpreter() throws FileNotFoundException, InstantiationException {
        OperatorFactory.initialize("operators.properties");
        context = new ExecutionContext();
        context.stack = new ExecutionStack(100);
        context.counter = 0;
        context.subroutines = new HashMap<>();

        instructionParser = new InstructionParser();
        subroutineParser = new SubroutineParser();
        lexer = new Lexer();
    }

    public void interpret(InputStream is) {
        Scanner scanner = new Scanner(is);
        List<Instruction> instructions = new ArrayList<>();
        while (scanner.hasNextLine()) {
            instructions.add(instructionParser.parse(lexer.tokenize(scanner.nextLine())));
        }

        List<Subroutine> subroutines = subroutineParser.parse(instructions);
        program = new ArrayList<>();

        for (Subroutine subroutine : subroutines) {
            context.subroutines.put(subroutine.getName(), subroutine);
            program.addAll(subroutine.getInstructions());
        }

        context.stack.push(new StackFrame("main",
                context.subroutines.get("main").getStartAddress()));
    }

    public void runNext() {
        if (finished) return;

        Instruction nextInst = program.get(context.counter);
        context.counter = getNextCommandAddr();
        ((Command) nextInst).execute(context); // context.counter can be changed if CALL is executed

        if (context.counter == -1) {
            finished = true;
        }
    }

    public ExecutionContext getContext() {
        return context;
    }

    public boolean finished() {
        return finished;
    }


    public Instruction getNextInstruction() {
        if (context.counter == -1) return null;
        Instruction inst = program.get(context.counter);
        if (inst.getName().equals("ret")) return null;
        return inst;
    }


    // Prediction of the next instruction
    public int getNextCommandAddr() {
        Subroutine sub = context.subroutines.get(getCurrentSubName());
        if (context.counter < sub.getStartAddress() + sub.getSize() - 1) {
            return context.counter + 1;
        } else {
            StackFrame frame = context.stack.pop();
            if (context.stack.isEmpty()) {
                context.stack.push(frame);
                return -1;
            }
            int ret = context.stack.top().getReturnAddress();
            context.stack.push(frame);
            return ret;
        }
    }

    private String getCurrentSubName() {
        return context.stack.top().getSubName();
    }


    public int getLineNumber(int address) {
        return program.get(address).getLineNumber();
    }


    public Set<Map.Entry<String, Integer>> getVariables() {
        return context.defineTable.entrySet();
    }
}
