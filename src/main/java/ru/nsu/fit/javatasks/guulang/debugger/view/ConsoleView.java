package ru.nsu.fit.javatasks.guulang.debugger.view;

public class ConsoleView extends IOStreamView {

    public ConsoleView() {
        super(System.in, System.out);
    }
}
