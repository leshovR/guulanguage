package ru.nsu.fit.javatasks.guulang.operators;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.nsu.fit.javatasks.guulang.interpreter.ExecutionStack;
import ru.nsu.fit.javatasks.guulang.interpreter.StackFrame;
import ru.nsu.fit.javatasks.guulang.parser.Subroutine;

import java.io.IOException;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

public class CallTest extends OperatorTest {
    private Call caller;
    private Subroutine sub;

    public CallTest() throws IOException {
        caller = new Call();
        sub = new Subroutine("foo", 1);
        context.subroutines = new HashMap<>();
        context.subroutines.put("main",new Subroutine("main", 0));
    }

    @BeforeEach
    public void fillStack() {
        context.stack = new ExecutionStack(2);
        context.stack.push(new StackFrame("main", 0));
    }

    @Test
    public void shouldCallSubroutine() {
        context.subroutines.put("foo", sub);
        caller.execute(context, new Object[]{"foo"});

        assertFalse(context.stack.isEmpty());
        StackFrame frame = context.stack.pop();
        assertNotNull(frame);
        assertEquals("foo", frame.getSubName());
        assertEquals(1, frame.getReturnAddress());
        assertEquals(1, context.counter);
    }

    @Test
    public void shouldThrowWhenPutUnknownSubName() {
        assertThrows(IllegalArgumentException.class,
                () -> caller.execute(context, new Object[]{"foo"}));
    }

    @Test
    public void shouldThrowWithoutArguments() {
        assertThrows(IllegalArgumentException.class,
                () -> caller.execute(context, new Object[]{}));
    }


    @Test
    public void shouldMakeRecursiveCall() {
        caller.execute(context, new Object[]{"main"});

        // Expect to get 2 frames with same content
        for (int i = 0; i < 2; ++i) {
            assertFalse(context.stack.isEmpty());
            StackFrame frame = context.stack.pop();
            assertNotNull(frame);
            assertEquals("main", frame.getSubName());
            assertEquals(0, frame.getReturnAddress());
            assertEquals(0, context.counter);
        }
    }
}