package ru.nsu.fit.javatasks.guulang.parser;

import ru.nsu.fit.javatasks.guulang.operators.OperatorFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/*
 * Groups instructions into the subroutines
 * and converts them into an executable command sequence
 */
public class SubroutineParser {

    public List<Subroutine> parse(List<Instruction> instructions) {
        List<Subroutine> result = new ArrayList<>();


        Subroutine sub = null;
        int i = 0;
        int ln = 0;
        for (Instruction inst : instructions) {
            ++ln;
            if (inst == null) {
                continue;
            }
            inst.setLineNumber(ln);

            if (inst.getType() == InstructionType.KEYWORD &&
                    inst.getName().equals("sub")) {
                if (sub != null) {
                    Command returnCommand = new Command(OperatorFactory.getInstance("ret"),
                            Collections.emptyList());
                    int line = sub.getInstructions()
                            .get(sub.getInstructions().size() - 1)
                            .getLineNumber();
                    returnCommand.setLineNumber(line + 1);
                    sub.addInstruction(returnCommand);
                    ++i;
                    result.add(sub);
                }
                sub = createSubroutine(i, inst);
            } else {
                if (sub == null) {
                    throw new IllegalArgumentException("Subroutine declaration is missing");
                }
                ++i;
                sub.addInstruction(inst);
            }
        }

        if (sub != null) {
            Command returnCommand = new Command(OperatorFactory.getInstance("ret"),
                    Collections.emptyList());
            returnCommand.setLineNumber(sub.getStartAddress() + sub.getSize() + 1);
            sub.addInstruction(returnCommand);

            result.add(sub);
        }

        return result;
    }


    private Subroutine createSubroutine(int start, Instruction inst) {
        Subroutine sub;
        List<Object> args = inst.getArgs();
        if (args.size() != 1 || !(args.get(0) instanceof String)) {
            throw new IllegalArgumentException("Wrong usage of sub");
        }
        sub = new Subroutine((String)args.get(0), start);
        return sub;
    }
}
