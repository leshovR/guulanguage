package ru.nsu.fit.javatasks.guulang.operators;

import ru.nsu.fit.javatasks.guulang.interpreter.ExecutionContext;
import ru.nsu.fit.javatasks.guulang.interpreter.StackFrame;

public class Ret implements Operator {
    @Override
    public void execute(ExecutionContext context, Object[] args) {
        if (args.length != 0) {
            throw new IllegalArgumentException("Wrong usage of ret");
        }

        context.stack.pop();

        if (!context.stack.isEmpty()) {
            StackFrame frame = context.stack.pop();
            context.counter = frame.getReturnAddress();
            context.stack.push(frame);
        } else {
            context.counter = -1;
        }
    }
}
