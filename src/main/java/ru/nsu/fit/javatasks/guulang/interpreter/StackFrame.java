package ru.nsu.fit.javatasks.guulang.interpreter;

/*
 * Single element of the stack
 * Stores subroutine name and return address.
 */

public class StackFrame {
    private String subName;
    private int retAddress;

    public StackFrame(String sub, int addr) {
        subName = sub;
        retAddress = addr;
    }

    public String getSubName() {
        return subName;
    }

    public void setReturnAddress(int addr) {
        retAddress = addr;
    }

    public int getReturnAddress() {
        return retAddress;
    }
}
