package ru.nsu.fit.javatasks.guulang.operators;

import ru.nsu.fit.javatasks.guulang.interpreter.ExecutionContext;

/*
 * Interface for classes which represent Guu language operators
 */
public interface Operator {
    void execute(ExecutionContext context, Object[] args);
}
