package ru.nsu.fit.javatasks.guulang.operators;

import ru.nsu.fit.javatasks.guulang.interpreter.ExecutionContext;

import java.util.Arrays;

public class Set implements Operator {
    @Override
    public void execute(ExecutionContext context, Object[] args) {
        if (args.length != 2) {
            throw new IllegalArgumentException(
                    "Wrong instruction format. Instruction: set, args: " +
                            Arrays.toString(args));
        }
        if (args[0] instanceof String) {
            String varName = (String) args[0];
            Object value = args[1];
            if (value instanceof Integer) {
                context.defineTable.put(varName, (Integer)value);
            } else if (value instanceof String) {
                Integer val = context.defineTable.getOrDefault(value, null);
                if (val == null) {
                    throw new IllegalArgumentException("Unknown variable name: " + value);
                }

                context.defineTable.put(varName, val);
            }
        } else {
            throw new IllegalArgumentException("Wrong argument format: \"" + args[0] + "\"");
        }
    }
}
