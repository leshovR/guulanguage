package ru.nsu.fit.javatasks.guulang.lexer;

import java.util.Objects;

/*
 * Basic lexeme class.
 */
public class Token {
    private TokenType type;
    private Object value;

    public Token(TokenType type, Object value) {
        this.type = type;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token token = (Token) o;
        return type == token.type &&
                value.equals(token.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, value);
    }

    public TokenType getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }
}
