package ru.nsu.fit.javatasks.guulang.debugger.commands;

import ru.nsu.fit.javatasks.guulang.debugger.view.View;
import ru.nsu.fit.javatasks.guulang.interpreter.ExecutionStack;
import ru.nsu.fit.javatasks.guulang.interpreter.Interpreter;
import ru.nsu.fit.javatasks.guulang.interpreter.StackFrame;

import java.io.IOException;
import java.util.ArrayList;

public class StackTrace implements DebuggerCommand {
    public void execute(Interpreter interpreter, View view) throws IOException {
        ExecutionStack stack = interpreter.getContext().stack;
        ArrayList<StackFrame> frames = new ArrayList<>();

        StackFrame topFrame = stack.pop();
        int topLine = interpreter.getLineNumber(interpreter.getContext().counter);
        view.printLine(topFrame.getSubName() + " at line " + topLine);

        while (!stack.isEmpty()) {
            StackFrame frame = stack.pop();
            frames.add(frame);
            interpreter.getContext().subroutines.get(frame.getSubName());
            int line = interpreter.getLineNumber(frame.getReturnAddress());
            view.printLine(frame.getSubName() + " at line " + (line - 1));
        }

        for (int i = frames.size() - 1; i >= 0; i--) {
            stack.push(frames.get(i));
        }
        stack.push(topFrame);
    }
}
