package ru.nsu.fit.javatasks.guulang.debugger.view;

import java.io.IOException;
import java.io.OutputStream;

/*
 * Interface for classes which
 * should display the information
 * and get user input.
 */
public interface View {
    void printLine(String line) throws IOException;
    String readLine() throws IOException;

    boolean finished();

    OutputStream getOutputStream();
}
