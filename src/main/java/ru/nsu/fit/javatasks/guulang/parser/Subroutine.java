package ru.nsu.fit.javatasks.guulang.parser;

import java.util.ArrayList;
import java.util.List;

/*
 * Stores information about subroutines.
 * Result of the work of subroutine parser.
 */

public class Subroutine{
    private String name;
    private int start;
    private List<Instruction> program;

    public Subroutine(String subName, int start) {
        name = subName;
        this.start = start;
        program = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public int getStartAddress() {
        return start;
    }

    public void addInstruction(Instruction instruction) {
        program.add(instruction);
    }

    public List<Instruction> getInstructions() {
        return program;
    }

    public int getSize() {
        return program.size();
    }
}
