package ru.nsu.fit.javatasks.guulang.lexer;

public enum TokenType {
    OPERATOR,
    NAME,
    NUMBER,
    KEYWORD
}
