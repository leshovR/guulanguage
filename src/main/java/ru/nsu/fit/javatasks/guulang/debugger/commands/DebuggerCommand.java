package ru.nsu.fit.javatasks.guulang.debugger.commands;

import ru.nsu.fit.javatasks.guulang.debugger.view.View;
import ru.nsu.fit.javatasks.guulang.interpreter.Interpreter;

import java.io.IOException;

public interface DebuggerCommand {
    void execute(Interpreter interpreter, View view) throws IOException;
}
