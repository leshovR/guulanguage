package ru.nsu.fit.javatasks.guulang.debugger.commands;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

/*
 * Used to get a command instance.
 * Loads command names from the file
 */
public class DebugCommandFactory {
    private static HashMap<String, DebuggerCommand> commands = new HashMap<>();

    private DebugCommandFactory() { }

    public static void initialize(String filename) throws FileNotFoundException, InstantiationException {
        Properties properties = new Properties();
        try {
            InputStream is = DebugCommandFactory.class.getClassLoader()
                    .getResourceAsStream(filename);

            properties.load(is);
            for (String opName : properties.stringPropertyNames()) {
                Class opClass = Class.forName(properties.getProperty(opName));
                DebuggerCommand command = (DebuggerCommand) opClass.newInstance();
                commands.put(opName, command);
            }
        } catch (IOException e) {
            FileNotFoundException fe = new FileNotFoundException("Can't initialize debugger");
            fe.addSuppressed(e);
            throw fe;
        } catch (Exception e) {
            InstantiationException ie = new InstantiationException("Invalid operators properties file");
            ie.addSuppressed(e);
            throw ie;
        }
    }

    public static DebuggerCommand getInstance(String name) {
        return commands.getOrDefault(name, null);
    }
}
