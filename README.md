# Build instruction
You can build this project using Apache Maven.
# Execution details
Program requires one filename argument passed to it via command line.  
Program communicates with user through the command line.  
# Debugger Commands

* **i** - step into  
* **o** - step over  
* **trace** - print stack trace  
* **var** - print all variables  

# Guu language

* **sub subname** - create new subroutine called *"subname"*  
* **call subname** - call the subroutine *"subname"*  
* **set varname value** - set the variable *"varname"* to the *value* which can be either integer or other variable name.  
* **print value** - print the value of integer or a variable  

