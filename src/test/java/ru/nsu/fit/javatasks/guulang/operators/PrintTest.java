package ru.nsu.fit.javatasks.guulang.operators;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PrintTest extends OperatorTest {
    private Print printer;

    public PrintTest() throws IOException {
        printer = new Print();
    }

    @Test
    public void shouldPrintNumber() {
        printer.execute(context, new Object[]{ 1 });
        assertEquals(1, scanner.nextInt());
    }

    @Test
    public void shouldPrintVariable() {
        context.defineTable.put("a", 1);
        printer.execute(context, new Object[]{ "a" });
        assertEquals(1, scanner.nextInt());
    }

    @Test
    public void shouldThrowWhenPutIncorrectNumber() {
        assertThrows(IllegalArgumentException.class,
                () -> printer.execute(context, new Object[]{1.25}),
                "Wrong argument format: \"1.25\"");
    }

    @Test
    public void shouldThrowWhenPutNotNumber() {
        assertThrows(IllegalArgumentException.class,
                () -> printer.execute(context, new Object[]{"1"}),
                "Wrong argument format: \"1\"");
    }

    @Test
    public void shouldThrowWhenPutMultipleArgs() {
        Object[] args = new Object[]{1, 2};
        assertThrows(IllegalArgumentException.class,
                () -> printer.execute(context, args),
                "Wrong instruction format. Instruction: printLine, args: " +
                        Arrays.toString(args));
    }
}