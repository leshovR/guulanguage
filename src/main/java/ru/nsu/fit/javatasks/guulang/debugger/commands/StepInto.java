package ru.nsu.fit.javatasks.guulang.debugger.commands;

import ru.nsu.fit.javatasks.guulang.debugger.view.View;
import ru.nsu.fit.javatasks.guulang.interpreter.Interpreter;

public class StepInto implements DebuggerCommand {
    @Override
    public void execute(Interpreter interpreter, View view) {
        interpreter.runNext();
        if (interpreter.getNextInstruction() == null) {
            interpreter.runNext();
        }
    }
}
