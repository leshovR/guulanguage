package ru.nsu.fit.javatasks.guulang;

import ru.nsu.fit.javatasks.guulang.debugger.Debugger;

public class Main {
    public static void main(String[] args) throws Exception {
        Debugger debugger = new Debugger();

        if (args.length != 1) {
            System.out.println("Please enter a single filename");
            return;
        }

        debugger.run(args[0]);
    }
}
