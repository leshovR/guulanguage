package ru.nsu.fit.javatasks.guulang.operators;

import ru.nsu.fit.javatasks.guulang.interpreter.ExecutionContext;
import ru.nsu.fit.javatasks.guulang.interpreter.StackFrame;

import java.util.Arrays;

public class Call implements Operator {
    @Override
    public void execute(ExecutionContext context, Object[] args) {
        if (args.length != 1) {
            throw new IllegalArgumentException(
                    "Wrong instruction format. Instruction: call, args: " +
                            Arrays.toString(args));
        }

        if (args[0] instanceof String) {
            String subName = (String) args[0];
            if (!context.subroutines.containsKey(subName)) {
                throw new IllegalArgumentException("Unknown subroutine name: " + subName);
            }

            // Set return address
            StackFrame currentFrame = context.stack.pop();
            currentFrame.setReturnAddress(context.counter);
            context.stack.push(currentFrame);

            // Push new stack frame with called subroutine info
            StackFrame newFrame = new StackFrame(subName, context.subroutines.get(subName).getStartAddress());
            context.stack.push(newFrame);

            context.counter = context.subroutines.get(subName).getStartAddress();
        } else {
            throw new IllegalArgumentException("Wrong argument format: \"" + args[0] + "\"");
        }
    }
}
