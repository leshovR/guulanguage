package ru.nsu.fit.javatasks.guulang.operators;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SetTest extends OperatorTest {
    private Set setter;
    public SetTest() throws IOException {
        setter = new Set();
    }

    @Test
    public void shouldSetVariableToInteger() {
        setter.execute(context, new Object[]{ "a", 1 });
        assertEquals((Integer)1, context.defineTable.get("a"));
    }

    @Test
    public void shouldSetVariableToAnotherVariable() {
        setter.execute(context, new Object[]{ "a", 1 });
        setter.execute(context, new Object[]{ "b", "a" });

        assertEquals((Integer)1, context.defineTable.get("a"));
        assertEquals((Integer)1, context.defineTable.get("b"));
    }
}