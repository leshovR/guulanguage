package ru.nsu.fit.javatasks.guulang.interpreter;


import ru.nsu.fit.javatasks.guulang.parser.Subroutine;

import java.io.OutputStream;
import java.util.HashMap;


public class ExecutionContext {
    public OutputStream out;
    public HashMap<String, Integer> defineTable = new HashMap<>();
    public HashMap<String, Subroutine> subroutines;
    public int counter;

    public ExecutionStack stack;
}
