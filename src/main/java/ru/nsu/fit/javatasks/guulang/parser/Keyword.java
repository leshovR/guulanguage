package ru.nsu.fit.javatasks.guulang.parser;

import java.util.List;

/*
 * Represents a keyword instruction.
 * Can't be executed.
 * Should be handled in a different way.
 */

public class Keyword implements Instruction {
    private String name;
    private List<Object> args;
    private int lineNumber;

    public Keyword(String name, List<Object> args) {
        this.name = name;
        this.args = args;
    }

    @Override
    public InstructionType getType() {
        return InstructionType.KEYWORD;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getLineNumber() {
        return lineNumber;
    }

    @Override
    public void setLineNumber(int number) {
        lineNumber = number;
    }

    public List<Object> getArgs() {
        return args;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getName());
        for (Object arg : getArgs()) {
            sb.append(" ").append(arg);
        }
        return sb.toString();
    }
}
