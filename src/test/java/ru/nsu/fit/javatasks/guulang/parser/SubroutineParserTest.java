package ru.nsu.fit.javatasks.guulang.parser;

import org.junit.jupiter.api.Test;
import ru.nsu.fit.javatasks.guulang.lexer.Lexer;
import ru.nsu.fit.javatasks.guulang.operators.OperatorFactory;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SubroutineParserTest {
    private SubroutineParser sParser;

    public SubroutineParserTest() throws FileNotFoundException, InstantiationException {
        OperatorFactory.initialize("operators.properties");
        sParser = new SubroutineParser();
    }

    @Test
    public void shouldParseText() {
        InstructionParser iParser = new InstructionParser();
        Lexer lexer = new Lexer();
        try {
            List<Instruction> instructions = new ArrayList<>();

            InputStream is = new FileInputStream("src/test/java/ru/nsu/fit/javatasks/guulang/parser/parserTest");
            Scanner sc = new Scanner(new BufferedInputStream(is));
            while(sc.hasNextLine()) {
                Instruction inst = iParser.parse(lexer.tokenize(sc.nextLine()));
                if (inst != null)
                    instructions.add(inst);
            }

            List<Subroutine> subroutines = sParser.parse(instructions);

            assertEquals(2, subroutines.size());
            Subroutine first = subroutines.get(0);
            Subroutine second = subroutines.get(1);
            assertEquals("main", first.getName());
            assertEquals("foo", second.getName());

            assertEquals(4, first.getInstructions().size());
            assertEquals(2, second.getInstructions().size());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}