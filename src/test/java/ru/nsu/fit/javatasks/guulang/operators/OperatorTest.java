package ru.nsu.fit.javatasks.guulang.operators;

import ru.nsu.fit.javatasks.guulang.interpreter.ExecutionContext;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.HashMap;
import java.util.Scanner;

public class OperatorTest {
    protected ExecutionContext context;
    protected Scanner scanner;

    public OperatorTest() throws IOException {
        PipedInputStream is = new PipedInputStream();
        context = new ExecutionContext();
        context.out = new PipedOutputStream(is);
        scanner = new Scanner(is);

        context.defineTable = new HashMap<>();
    }
}